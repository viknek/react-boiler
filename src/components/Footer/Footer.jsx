import React from 'react';
import PropTypes from 'prop-types';

import styles from './Footer.scss';

const Footer = ({text}) => {
  console.log(styles);

  return (
    <footer className={`${styles.container}`}>
      { text }
    </footer>
  )
};

Footer.propTypes = {
  text: PropTypes.string.isRequired,
};

export default Footer;