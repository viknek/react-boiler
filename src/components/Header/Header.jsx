import React from 'react';
import { NavLink } from 'react-router-dom';
import styles from './Header.scss';

const Header = () => {
  console.log(styles);

  return (
    <header className={ `${ styles.container }` }>
      <NavLink className={ `${ styles.link }` }
               to='/'
               activeClassName={ `${ styles.activeLink }` }
               exact>
       Home
      </NavLink>
      <NavLink className={ `${ styles.link }` }
               to='/contact'
               activeClassName={ `${ styles.activeLink }` }
               exact>
        Contact
      </NavLink>
    </header>
  )
};


export default Header;