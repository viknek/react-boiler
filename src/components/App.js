import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import Footer from './Footer/Footer';
import Header from './Header/Header';
import Home from './Home/Home';
import Contact from './Contact/Contact';

import styles from './App.scss';

const App = () => {
  return (
    <div className={ `${ styles.wrapper }` }>
      <Header/>
      <Switch>
        <Route path='/' component={ Home } exact />
        <Route path='/contact' component={ Contact } />
      </Switch>
      <Footer text={ "Footer Placeholder" }/>
    </div>
  );
};

export default App;