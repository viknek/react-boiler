import React from 'react';
import styles from './Home.scss';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchData } from '../../actions/dataActions';

class Home extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    console.log(this.props);
  }

  render() {
    const { data: { payload } } = this.props;

    return (
      <div className={ `${ styles.container }` }>
        {
          payload.map((item, index) => (
            <div key={index}>{ item.name }</div>
          ))
        }
      </div>
    )
  }
}

function mapStateToProps({ data }) {
  return {
    data
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      fetchData
    }, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);