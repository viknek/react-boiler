import { FETCH_DATA } from '../../actions/types';
import initialState from './initialState';

export default(state = initialState, action) => {
  switch(action.type) {
    case FETCH_DATA:
      return Object.assign({}, state, {
        payload: action.payload
      });
    default:
      return state;
  }
}
