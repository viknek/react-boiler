import { FETCH_DATA } from './types';

export function fetchData(payload) {
  return { type: FETCH_DATA, payload };
}
